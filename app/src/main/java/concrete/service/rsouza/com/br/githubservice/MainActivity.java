package concrete.service.rsouza.com.br.githubservice;

import android.app.ProgressDialog;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {


    ListView listview;
    RepositoryListAdapter adapter;

    JSONObject jsonobject;
    JSONArray jsonarray;
    JSONFunctions jSONFunctions ;
    ArrayList<HashMap<String, String>> arraylist;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    ProgressDialog mProgressDialog;

    static String REPOSITORYNAME = "repositoryName";
    static String REPOSITORYDESCRIPTION = "repositoryDescription";
    static String STARCOUNT = "starCount";
    static String FORKCOUNT = "forkCount";
    static String AVATAR = "ownerPicture";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_main);

        // DownloadJSON AsyncTask
        new DownloadJSON().execute();
    }


    public class DownloadJSON extends AsyncTask<Void, Void, Void> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MainActivity.this);

            mProgressDialog.setTitle("Concrete");
            mProgressDialog.setMessage("Aguarde...");
            //mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            arraylist = new ArrayList<HashMap<String, String>>();
            //url json
            jsonobject = jSONFunctions.getJson("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1");
            https://github.com/rogeriosouza?tab=repositories
            try {
                JSONObject data = jsonobject.getJSONObject("items");
                JSONArray hotTopics = data.getJSONArray("owner");

                for (int i = 0; i < hotTopics.length(); i++) {

                    JSONObject topic = hotTopics.getJSONObject(i).getJSONObject("items");

                    HashMap<String, String> map = new HashMap<String, String>();

                    // Retrive JSON Objects
                    if (topic.getString("name") !=null ){
                        map.put("repositoryName", topic.getString("name"));
                    }else{
                        map.put("repositoryName", " ");

                    }
                    if (topic.getString("description") !=null ){
                        map.put("repositoryDescription", topic.getString("description"));
                    }else{
                        map.put("repositoryDescription", "");

                    }
                    if ((topic.getString("stargazers_count")) !=null ){
                        map.put("starCount", topic.getString("stargazers_count"));
                    }else{
                        map.put("starCount", "0");

                    }
                    if ((topic.getString("forks")) !=null ){
                        map.put("forkCount", topic.getString("forks"));
                    }else{
                        map.put("forkCount", "0");

                    }
                    map.put("ownerPicture", topic.getString("avatar_url"));


                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }





            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }



            protected void OnPostExecute(Void  s ){
/*
                mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

                mRecyclerView.setHasFixedSize(true);

                // specify an adapter (see also next example)criado novo constructor listadapter
                mAdapter = new RepositoryListAdapter(MainActivity.this, arraylist);
                mRecyclerView.setAdapter(mAdapter);*/


                listview = (ListView) findViewById(R.id.listview);

                adapter = new RepositoryListAdapter (MainActivity.this, arraylist);

                listview.setAdapter(adapter);
                mProgressDialog.dismiss();


        }

    }
}
