package concrete.service.rsouza.com.br.githubservice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ROGERIO on 17/06/2017.
 */


public class RepositoryListAdapter extends BaseAdapter {



    private final List<Repository> items = new ArrayList<>();

    ArrayList<HashMap<String, String>> data;
    LayoutInflater inflater;
    Context context;
    ImageLoader imageLoader;
    HashMap<String, String> resultGet = new HashMap<String, String>();

    public RepositoryListAdapter(Context context,
                                 ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        this.inflater =  inflater.from(context);
        data = arraylist;
        imageLoader = new ImageLoader(context);
    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {return null; }

    @Override
    public long getItemId(int position) { return 0;     }

    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView repositoryName;
        TextView repositoryDescription;
        TextView stars;
        TextView forks;
        ImageView avatar;
        View itemView = inflater.inflate(R.layout.listview_item, parent, false);


        resultGet = data.get(position);

        repositoryName = (TextView) itemView.findViewById(R.id.repositoryName);
        repositoryDescription = (TextView) itemView.findViewById(R.id.repositoryDescription);
        stars = (TextView) itemView.findViewById(R.id.starCount);
        forks = (TextView) itemView.findViewById(R.id.forkCount);
        avatar = (ImageView) itemView.findViewById(R.id.ownerPicture);


        repositoryName.setText(resultGet.get(MainActivity.REPOSITORYNAME));
        repositoryDescription.setText(resultGet.get(MainActivity.REPOSITORYDESCRIPTION));
        stars.setText(resultGet.get(MainActivity.STARCOUNT));
        forks.setText(resultGet.get(MainActivity.FORKCOUNT));
        imageLoader.DisplayImage(resultGet.get(MainActivity.AVATAR),avatar);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultGet = data.get(position);
                Intent intent = new Intent(context, PullRequestsActivity.class);

                intent.putExtra("repositoryname", resultGet.get(MainActivity.REPOSITORYNAME));

                intent.putExtra("repositorydescription", resultGet.get(MainActivity.REPOSITORYDESCRIPTION));

                intent.putExtra("forkCount",resultGet.get(MainActivity.FORKCOUNT));
                intent.putExtra("starCount",resultGet.get(MainActivity.STARCOUNT));

                intent.putExtra("ownerPicture", resultGet.get(MainActivity.AVATAR));

                context.startActivity(intent);


            }
        });





        return itemView;
    }



}
