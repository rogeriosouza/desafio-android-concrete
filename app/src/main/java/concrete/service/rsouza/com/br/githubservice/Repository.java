package concrete.service.rsouza.com.br.githubservice;

/**
 * Created by ROGERIO on 17/06/2017.
 */

public class Repository {


    private String name;
    private String description;
    private String stars;
    private String forks;
    private String avatar;

        public static Repository getNew(String name, String description, String stars, String forks,
                                    String ownerPicture) {

                Repository repository = new Repository();
                repository.setName(name);
                repository.setDescription(description);
                repository.setStars(stars);
                repository.setForks(forks);
                repository.setAvatar(ownerPicture);

              return repository;




            }

    @Override
    public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                        Repository that = (Repository) o;

                        if (name != null ? !name.equals(that.name) : that.name != null) return false;
                if (description != null ? !description.equals(that.description) : that.description != null)
                        return false;
                if (stars != null ? !stars.equals(that.stars) : that.stars != null) return false;
                if (forks != null ? !forks.equals(that.forks) : that.forks != null) return false;

                    return  false;
                    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getForks() {
        return forks;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
