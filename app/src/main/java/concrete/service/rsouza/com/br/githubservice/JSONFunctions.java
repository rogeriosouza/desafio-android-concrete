package concrete.service.rsouza.com.br.githubservice;

import com.google.gson.Gson;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ROGERIO on 17/06/2017.
 */

public class JSONFunctions {

    HttpURLConnection urlConnection = null;
    String result = "";
    JSONObject jArray = null;

    public  JSONObject getJson(final String url) {

        // Toda chamada externa necessita rodar em background, então utilizamos thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                Object retorno = null;
                try {


                    URL urlObj = new URL(url);
                    urlConnection = (HttpURLConnection) urlObj.openConnection();
                    urlConnection.setReadTimeout(3000);
                    urlConnection.setConnectTimeout(3000);

                    int responseCode = urlConnection.getResponseCode();
                    if (responseCode != HttpsURLConnection.HTTP_OK) {
                        throw new IOException("HTTP error code: " + responseCode);
                    }

                    InputStream is = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    // resposta to string
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();

                    // Aqui  Biblioteca Gson para transformar o Json recebido em Objeto JAVA
                    /* Instancia o objeto Gson e em seguida utilizamos o método fromJson() passando como parâmetro o Reader instanciado e
                    o tipo do Objeto que será retornado. */
                    Gson gson = new Gson();
                    retorno = gson.fromJson(result, HashMap.class);
                    is.close();
                    jArray = new JSONObject(String.valueOf(retorno));

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }).start();

        return jArray;

    }


}
